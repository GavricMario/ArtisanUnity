﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealOrb : MonoBehaviour
{

    [SerializeField] private int healAmount = 10;

    private bool playerHealed = false;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && !playerHealed)
        {
            playerHealed = true;
            collision.GetComponent<PlayerController>().Heal(healAmount);
            Destroy(gameObject);
        }
    }
}
