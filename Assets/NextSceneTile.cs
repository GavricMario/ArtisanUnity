﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextSceneTile : MonoBehaviour
{
    [SerializeField] private GameHandler handler;

    private void OnTriggerExit2D(Collider2D collision)
    {
        handler.GoToNextScene(1);
    }
}
