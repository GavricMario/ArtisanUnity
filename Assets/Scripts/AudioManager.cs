﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;
using System;
using System.Diagnostics;

public class AudioManager : MonoBehaviour
{
    public Sounds[] sounds;
    public static AudioManager instance;
    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
        //DontDestroyOnLoad(gameObject); ostaje izmedu levela
        foreach (Sounds s in sounds) 
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
        
    }
    void Start()
    {
        Play("Background");
    }

    public void Play(string name) 
    {
        Sounds s = Array.Find(sounds, sound => sound.name == name);
        if( s == null)
        {
            UnityEngine.Debug.LogWarning("sounds name might be wrong");
            return;
        }

        s.source.Play();


        //FindObjectOfType<AudioManager>().Play("name"); tako se poziva iz bilo koje skripte, npr za attack

    }

    public void Stop(string name)
    {
        Sounds s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            UnityEngine.Debug.LogWarning("sounds name might be wrong");
            return;
        }

        s.source.Stop();
    }
}
