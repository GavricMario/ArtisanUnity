﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitch : MonoBehaviour
{
    [SerializeField] private string cameraOneName = "BossCam";
    [SerializeField] private string cameraTwoName = "MovementCam";
    // Start is called before the first frame update
    public void SwitchCamera(float delay)
    {
        Invoke("Switch", delay);
    }

    private void Switch()
    {
        transform.Find(cameraOneName).gameObject.SetActive(false);
        transform.Find(cameraTwoName).gameObject.SetActive(true);
        Invoke("ScaleGUI", 4f);
    }

    private void ScaleGUI()
    {
        transform.Find("PlayerStatsGUI").localScale = new Vector3(1, 1, 1);
        transform.Find("PlayerStatsGUI").localPosition = new Vector3(-11f, 6.5f, 0f);
    }

    public void ShowBossStats()
    {
        transform.Find("BossStatsGUI").localScale = new Vector3(0.5f, 0.5f, 1);
        transform.Find("BossStatsGUI").localPosition = new Vector3(6, -2f, 0f);
        //transform.Find("Preview").gameObject.SetActive(true);
    }
}
