﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowStats : MonoBehaviour
{
    [SerializeField] private CameraSwitch camera;
    [SerializeField] private GameObject bossImage;
    [SerializeField] private CovidBossController covidBoss;

    private Parallax parallax;

    private void Awake()
    {
        parallax = bossImage.GetComponent<Parallax>();
        Destroy(bossImage.GetComponent<Parallax>());
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.name == "BossDead")
        {
            Destroy(bossImage.GetComponent<BoxCollider2D>());
            Destroy(bossImage.GetComponent<Rigidbody2D>());
            bossImage.transform.localPosition = new Vector3(0f, -72f, 30f);
            bossImage.GetComponent<SpriteRenderer>().color = new Color(0.2980392f, 0.4235294f, 0.4392157f, 1f);
            bossImage.AddComponent<Parallax>();
            bossImage.GetComponent<Parallax>().cam = parallax.cam;
            bossImage.GetComponent<Parallax>().parallexEffect = 0.5f;
        }
        else if (collision.CompareTag("Player"))
        {
            FindObjectOfType<AudioManager>().Stop("Background2");
            FindObjectOfType<AudioManager>().Play("Background3");
            StartCoroutine(SpawnCorona());
        }
    }

    IEnumerator SpawnCorona()
    {
        yield return new WaitForSeconds(7.3f);
        covidBoss.Spawn();
        camera.ShowBossStats();
    }
}
