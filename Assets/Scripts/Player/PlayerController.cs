﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
	[Header("Movement")]
	[SerializeField] public float runSpeed = 40f;
	[SerializeField] private float m_JumpForce = 15f;                           // Amount of force added when the player jumps.
    [SerializeField] public float fallDamageSpeed = 25f;
	[SerializeField] private float jumpTime;
	[Range(0, 1)] [SerializeField] private float m_CrouchSpeed = .36f;          // Amount of maxSpeed applied to crouching movement. 1 = 100%
	[Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;  // How much to smooth out the movement
	[SerializeField] private bool m_AirControl = false;                         // Whether or not a player can steer while jumping;
	[Space]
	[Header("Stats")]
	[SerializeField] public float startHealAfterTime = 4f;
	[SerializeField] public int healingAmountPerSecond = 2;
	[SerializeField] private int maxHP = 100;
	[SerializeField] private int maxMana = 100;
	[Space]
	[Header("Attacking")]
	[SerializeField] public float attackCooldown;
	[SerializeField] public float attackButtonPressRate;
	[SerializeField] public float maxAttackCombo;
	[SerializeField] private float lightAttackRange;
	[SerializeField] private float lightAttackDamage;
	[SerializeField] public float heavyAttackCooldown;
	[SerializeField] private int heavyAttackManaCost = 33;
	[SerializeField] private int lightAttackHPCost = 2;
	[SerializeField] private int heavyAttackHPCost = 5;
	[SerializeField] private float throwDistance = 10f;
	[SerializeField] private LayerMask enemyLayers;
	[Space]
	[Header("Objects")]
	[SerializeField] private LayerMask m_WhatIsGround;                          // A mask determining what is ground to the character
	[SerializeField] private Transform m_GroundCheck;                           // A position marking where to check if the player is grounded.
	[SerializeField] private Transform m_CeilingCheck;                          // A position marking where to check for ceilings
	[SerializeField] private Collider2D m_CrouchDisableCollider;                // A collider that will be disabled when crouching
	[SerializeField] private PlayerParticleController particleController;
	[SerializeField] private Bar healthBar;
	[SerializeField] private Bar manaBar;
	[SerializeField] private Animator animator;
	[SerializeField] private Transform attackPoint;
	[SerializeField] private Transform throwPosition;
	[SerializeField] private Rigidbody2D projectileObject;

	private int currentHP;
	private int currentMana;
	private bool isHealingRunning = false;
	private float lastManaUpdate;
	private float fallingSpeed = 0f;

	private float poisonTime = 0;
	private float poisonDamage = 0;
	private bool poisonRunning = false;

	private float jumpTimeCounter;
	private bool isJumping = false;

	private bool isHeavAttack = false;

	const float k_GroundedRadius = .25f;                               // Radius of the overlap circle to determine if grounded
	private bool m_Grounded;                                          // Whether or not the player is grounded.
	private bool m_AtCeiling;
	const float k_CeilingRadius = .15f;                                // Radius of the overlap circle to determine if the player can stand up
	private Rigidbody2D m_Rigidbody2D;
	private bool m_FacingRight = true;                                // For determining which way the player is currently facing.
	private Vector3 m_Velocity = Vector3.zero;

	private Vector3 target;

	[System.Serializable]
	public class BoolEvent : UnityEvent<bool> { }

	[System.Serializable]
	public class FloatEvent : UnityEvent<float> { }

	[Header("Events")]
	[Space]
	public FloatEvent OnLandEvent;
	public BoolEvent OnCrouchEvent;

	private bool m_wasCrouching = false;

	private void Start()
	{
		currentHP = maxHP;
		currentMana = maxMana / 2;
		healthBar.SetSize(currentHP / maxHP);
		manaBar.SetSize(currentMana / maxMana);
	}

	private void Awake()
	{
		m_Rigidbody2D = GetComponent<Rigidbody2D>();

		if (OnLandEvent == null)
			OnLandEvent = new FloatEvent();

		if (OnCrouchEvent == null)
			OnCrouchEvent = new BoolEvent();
	}

	private void Update()
	{
		target = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		Bounds bounds = GetComponent<SpriteRenderer>().bounds;
		Bounds boundsCircle = GetComponent<CircleCollider2D>().bounds;
		Bounds boundsBox = GetComponent<BoxCollider2D>().bounds;
		bounds.Encapsulate(boundsCircle);
		bounds.Encapsulate(boundsBox);
		bounds.Expand(3);
		AstarPath.active.UpdateGraphs(bounds);

		checkMana();
		checkColisions();
		checkJumping();
	}

	private void checkColisions()
	{
		bool wasGrounded = m_Grounded;
		m_Grounded = false;

		if (Physics2D.OverlapCircle(m_CeilingCheck.position, k_CeilingRadius, m_WhatIsGround))
		{
			m_AtCeiling = true;
		}
		else
		{
			m_AtCeiling = false;
		}

		// The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
		Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
		foreach (Collider2D collider in colliders)
		{
			if (collider.gameObject != gameObject && !collider.isTrigger)
			{
				m_Grounded = true;
				if (!wasGrounded)
				{
					Landed();
				}
			}
		}
	}

	private void checkJumping()
	{

		if (!m_Grounded && m_Rigidbody2D.velocity.y < 0)
		{
			animator.SetBool("isFalling", true);
			fallingSpeed = m_Rigidbody2D.velocity.y;
		}

		if (Input.GetButtonUp("Jump") || m_AtCeiling)
		{
			isJumping = false;
		}
		if (Input.GetButton("Jump") && !m_Grounded && isJumping)
		{
			if (jumpTimeCounter > 0)
			{
				jumpTimeCounter -= Time.deltaTime;
				m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, (Vector2.up * m_JumpForce).y);
			}
			else
			{
				isJumping = true;
			}
		}
	}

	public void Move(float move, bool crouch, bool jump)
	{
		animator.SetFloat("speed", Mathf.Abs(move));

		// If crouching, check to see if the character can stand up
		if (crouch && m_Grounded)
		{
			//FindObjectOfType<AudioManager>().Play("name"); play crouch
		}

		if (crouch && !m_Grounded)
		{
			crouch = false;
		}
		else if (!crouch && m_Grounded)
		{
			if (Physics2D.OverlapCircle(m_CeilingCheck.position, k_CeilingRadius, m_WhatIsGround))
			{
				crouch = true;
			}
			if (isHeavAttack)
			{
				crouch = true;
			}
		}



		animator.SetBool("isCrouching", crouch && !isHeavAttack);

		if (m_Grounded || m_AirControl)
		{
			if (crouch)
			{
				if (!m_wasCrouching)
				{
					m_wasCrouching = true;
					OnCrouchEvent.Invoke(true);
				}

				// Reduce the speed by the crouchSpeed multiplier
				move *= m_CrouchSpeed;

				// Disable one of the colliders when crouching
				if (m_CrouchDisableCollider != null)
					m_CrouchDisableCollider.enabled = false;
			}
			else
			{
				// Enable the collider when not crouching
				if (m_CrouchDisableCollider != null)
					m_CrouchDisableCollider.enabled = true;

				if (m_wasCrouching)
				{
					m_wasCrouching = false;
					OnCrouchEvent.Invoke(false);
				}
			}

			// Move the character
			Vector3 targetVelocity = new Vector2(move * 10f, m_Rigidbody2D.velocity.y);
			m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);

			// Check player rotation
			if (move > 0 && !m_FacingRight && !isHeavAttack)
			{
				Flip();
			}
			else if (move < 0 && m_FacingRight && !isHeavAttack)
			{
				Flip();
			}
		}
		// If the player should jump...
		if (jump && m_Grounded && !m_AtCeiling)
		{
			FindObjectOfType<AudioManager>().Play("Jumping");
			m_Grounded = false;
			isJumping = true;
			jumpTimeCounter = jumpTime;
			animator.SetBool("isJumping", true);
			m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, (Vector2.up * m_JumpForce).y);
		}

		TrailRender(move, crouch);

	}

	private void TrailRender(float move, bool crouch)
	{
		if (move > 0 && !animator.GetBool("isJumping") && !animator.GetBool("isFalling") && !crouch)
		{
			particleController.playRunningToRightTrail();
		}
		else if (move < 0 && !animator.GetBool("isJumping") && !animator.GetBool("isFalling") && !crouch)
		{
			particleController.playRunningToLeftTrail();
		}
		else
		{
			particleController.stopParticles();
		}
	}

	private void Landed()
	{
		OnLandEvent.Invoke(fallingSpeed);
		particleController.playLandingDust();

		animator.SetBool("isFalling", false);
		animator.SetBool("isJumping", false);

		if (Mathf.Abs(fallingSpeed) > fallDamageSpeed)
		{
			OnDamage(((Mathf.Abs(fallingSpeed) / fallDamageSpeed) - 1) * 100);
		}
		fallingSpeed = 0;
	}

	private void checkMana()
	{
		if (Time.time - lastManaUpdate > 0.5f && currentMana < maxMana)
		{
			OnManaChanged(-5);
			lastManaUpdate = Time.time;

		}
	}

	public void OnDamage(float damage)
	{
		if (currentHP > 0)
		{
			FindObjectOfType<AudioManager>().Play("PlayerDamage");
			currentHP -= (int)damage;
			OnHealthChanged(true);
		}
	}

	public void OnDamage(int damage)
	{
		if (currentHP > 0)
		{
			FindObjectOfType<AudioManager>().Play("PlayerDamage");
			currentHP -= damage;
			OnHealthChanged(true);
		}
	}

	public void Poison(float poisonTime, float poisonDamagePerSecond)
	{
		this.poisonTime = poisonTime;
		poisonDamage = poisonDamagePerSecond;
		if (!poisonRunning)
		{
			poisonRunning = true;
			StartCoroutine(PoisonMe());
		}
	}

	IEnumerator PoisonMe()
	{
		while (poisonTime > 0)
		{
			Debug.Log(poisonTime);
			poisonTime -= Time.time;
			OnDamage(poisonDamage);
			yield return new WaitForSeconds(0.75f);
		}
		poisonRunning = false;
		yield return null;
	}

	public void Heal(int healAmount)
	{
		currentHP += healAmount;
		OnHealthChanged(false);
	}

	private void OnHealthChanged(bool hasTakenDamage)
	{
		if (currentHP < 0)
		{
			currentHP = 0;
		} if (currentHP > maxHP)
		{
			currentHP = maxHP;
		}

		healthBar.SetSize((float)currentHP / maxHP);

		if (currentHP < 1)
		{
			//TODO show game over menu
			FindObjectOfType<AudioManager>().Play("PlayerDeath");
			Destroy(GetComponent<PlayerMovement>());
			animator.SetTrigger("die");
			m_Rigidbody2D.velocity = Vector3.zero;
			Destroy(gameObject, 1.5f);
			Invoke("GameOver", 1f);
		} else if (hasTakenDamage)
		{
			animator.SetTrigger("takeDamage");
		}
	}

	private void GameOver()
	{
		SceneManager.LoadScene(5);
	}

	private void OnManaChanged(int manaChange)
	{
		currentMana -= manaChange;
		if (currentMana > maxMana)
		{
			currentMana = maxMana;
		}
		manaBar.SetSize((float)currentMana / maxMana);
	}

	public void ToggleHealing(bool isHealing)
	{
		if (isHealing && !isHealingRunning)
		{
			isHealingRunning = true;
			InvokeRepeating("Healing", 0f, 1f);
			particleController.ToggleHealing(isHealing);
		}
		else if (!isHealing)
		{
			if (isHealingRunning)
			{
				isHealingRunning = false;
				CancelInvoke("Healing");
				particleController.ToggleHealing(isHealing);
			}
		}

	}

	private void Healing()
	{
		currentHP += healingAmountPerSecond;
		OnHealthChanged(false);
	}

	public void LightAttack(int attackState)
	{
		if (attackState > 0)
		{
			FindObjectOfType<AudioManager>().Play("PrimaryAttack");
			animator.SetBool("isAttacking", true);
			if (attackState % 2 == 0)
			{
				animator.SetTrigger("LightAttack2");
			}
			else
			{
				animator.SetTrigger("LightAttack1");
			}

			currentHP -= lightAttackHPCost;
			OnHealthChanged(false);
			Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, lightAttackRange, enemyLayers);

			foreach (Collider2D enemy in hitEnemies)
			{
				if (enemy.CompareTag("Enemy")) {
					try
					{
						enemy.GetComponent<EnemyAIController>().OnDamage(lightAttackDamage);
					}
					catch (Exception) { }
					try
					{
						enemy.GetComponent<UndeadConroller>().OnDamage(lightAttackDamage);
					}
					catch (Exception) { }
				}
				else if (enemy.CompareTag("Boss"))
				{
					try
					{
						enemy.GetComponent<BossAIController>().OnDamage(lightAttackDamage);
					}
					catch (Exception) { }
				}
				else if (enemy.CompareTag("Chest"))
				{
					enemy.GetComponent<Chest>().OnDamage(lightAttackDamage);
				}

			}
		} else
		{
			animator.SetBool("isAttacking", false);
		}
	}

	public void HeavyAttack()
	{
		if (heavyAttackManaCost < currentMana)
		{
			isHeavAttack = true;
			OnManaChanged(heavyAttackManaCost);
			animator.SetTrigger("HeavyAttack");
			currentHP -= heavyAttackHPCost;
			OnHealthChanged(false);

			if (target.x < throwPosition.position.x && m_FacingRight)
			{
				Flip();
			}
			else if (target.x > throwPosition.position.x && !m_FacingRight)
			{
				Flip();
			}
			FindObjectOfType<AudioManager>().Play("Throw");
			StartCoroutine(Throw());

		} 
		else
		{
			GetComponent<PlayerAttack>().restartAttack();
		}
	}

	IEnumerator Throw()
	{
		yield return new WaitForSecondsRealtime(0.2f);
		Rigidbody2D projectile = Instantiate(projectileObject);
		projectile.transform.position = throwPosition.transform.position;
		Vector2 difference = target - transform.position;
		float distance = difference.magnitude;
		Vector2 direction = difference / distance;
		projectile.velocity = direction.normalized * throwDistance;
		isHeavAttack = false;
		yield return null;
	}

	private void OnDrawGizmosSelected()
	{
		if (attackPoint == null) return;
		Gizmos.DrawWireSphere(attackPoint.position, lightAttackRange);
		Gizmos.DrawWireSphere(m_GroundCheck.position, k_GroundedRadius);
		Gizmos.DrawWireSphere(m_CeilingCheck.position, k_CeilingRadius);
	}

	private void Flip()
	{
		// Switch the way the player is labelled as facing.
		m_FacingRight = !m_FacingRight;

		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
}