﻿using System.Collections;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{

    [SerializeField] private PlayerController controller;

    private int attackState;
    private float lastAttackTime = Mathf.NegativeInfinity;
    private float attackCooldown;
    private float maxTime;
    private float maxAttackCombo;
    private float heavyAttackCooldown;
    private float lastHeavyAttackTime = Mathf.NegativeInfinity;
    private bool isHeavyAttack;

    void Start()
    {
        attackCooldown = controller.attackButtonPressRate;
        maxTime = controller.attackCooldown;
        maxAttackCombo = controller.maxAttackCombo;

        heavyAttackCooldown = controller.heavyAttackCooldown;
        isHeavyAttack = false;

        StartCoroutine(Melee());
        StartCoroutine(Heavy());
    }

    public void restartAttack()
    {
        isHeavyAttack = false;
    }

    IEnumerator Melee()
    {
        //Constantly loops so you only have to call it once
        while (true)
        {
            //Checks if attacking and then starts of the combo
            if (Input.GetButtonDown("Fire1") && !isHeavyAttack)
            {
                attackState++;
                controller.LightAttack(attackState);
                lastAttackTime = Time.time;

                //Combo loop that ends the combo if you reach the maxTime between attacks, or reach the end of the combo
                while ((Time.time - lastAttackTime) < maxTime && attackState < maxAttackCombo)
                {
                    //Attacks if your cooldown has reset
                    if (Input.GetButtonDown("Fire1") && (Time.time - lastAttackTime) > attackCooldown && !isHeavyAttack)
                    {
                        attackState++;
                        controller.LightAttack(attackState);
                        lastAttackTime = Time.time;
                        yield return new WaitForSeconds(0.15f);
                    }
                    yield return null;
                }
                //Resets combo and waits the remaining amount of cooldown time before you can attack again to restart the combo
                yield return new WaitForSeconds(attackCooldown - (Time.time - lastAttackTime) + 0.06f);
                //yield return new WaitForSeconds(maxTime - (Time.time - lastAttackTime) + 0.06f);
                attackState = 0;
                controller.LightAttack(attackState);
            }
            yield return null;

        }
    }

    IEnumerator Heavy()
    {
        while(true)
        {
            if (Input.GetButtonDown("Fire2") && Time.time - heavyAttackCooldown > lastHeavyAttackTime)
            {
                isHeavyAttack = true;
                lastHeavyAttackTime = Time.time;
                yield return new WaitForSeconds(0.3f);
                controller.HeavyAttack();
            } else if (Time.time - lastHeavyAttackTime > 0.6f)
            {
                isHeavyAttack = false;
            }
            yield return null;
        }
    }
}
