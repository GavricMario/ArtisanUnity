﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    [SerializeField] private PlayerController controler;

    private float runSpeed;
    private float fallDamageSpeed;
    private float horizontalMove = 0f;
    private bool jump = false;
    private bool crouch = false;

    private float timeFreezed = 0;
    private bool isFrozen = false;
    private bool isStanding = false;
    private float standingTime = 0f;
    private float startHealAfterTime; 

    private void Start()
    {
        fallDamageSpeed = controler.fallDamageSpeed;
        runSpeed = controler.runSpeed;
        startHealAfterTime = controler.startHealAfterTime;
    }

    void Update()
    {
        if (!isFrozen)
        {
            horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
        } else if (timeFreezed < Time.time)
        {
            isFrozen = false;
        }

        if (isStanding && Time.time - standingTime > startHealAfterTime)
        {
            controler.ToggleHealing(true);
        }
        else if (!isStanding)
        {
            controler.ToggleHealing(false);
        }

        if (Input.GetButtonDown("Jump"))
        {
            jump = true;
        }

        if (Input.GetButtonDown("Crouch"))
        {
            crouch = true;
        } 
        else if (Input.GetButtonUp("Crouch"))
        {
            crouch = false;
        }
    }

    public void OnLanding(float fallingSpeed)
    {
        if (fallingSpeed < -fallDamageSpeed)
        {
            horizontalMove = 0;
            timeFreezed = Time.time + 0.5f;
            isFrozen = true;
        }
    }

    void FixedUpdate()
    {
        if (Mathf.Abs(horizontalMove) < 0.1f && !isStanding)
        {
            standingTime = Time.time;
            isStanding = true;
        } 
        else if (Mathf.Abs(horizontalMove) > 0.1f && isStanding)
        {
            isStanding = false;
        }
        controler.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
        jump = false;
    }
}
