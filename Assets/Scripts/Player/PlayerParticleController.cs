﻿using UnityEngine;


public class PlayerParticleController : MonoBehaviour
{
	[SerializeField] private ParticleSystem runningDustTrailRight;
	[SerializeField] private ParticleSystem runningDustTrailLeft;
	[SerializeField] private ParticleSystem landingDust;
	[SerializeField] private ParticleSystem healingParticles;

	public void playRunningToRightTrail()
	{
		if (runningDustTrailLeft.isPlaying) runningDustTrailLeft.Stop();
		runningDustTrailLeft.Clear();
		if (!runningDustTrailRight.isPlaying) runningDustTrailRight.Play();
	}

	public void playRunningToLeftTrail()
	{
		if (runningDustTrailRight.isPlaying) runningDustTrailRight.Stop();
		runningDustTrailRight.Clear();
		if (!runningDustTrailLeft.isPlaying) runningDustTrailLeft.Play();
	}

	public void playLandingDust()
	{
		ParticleSystem.EmitParams emitOverride = new ParticleSystem.EmitParams();
		emitOverride.startLifetime = 0.3f;
		if (!landingDust.isPlaying) landingDust.Emit(emitOverride, 1);
	}

	public void stopParticles()
	{
		runningDustTrailRight.Stop();
		runningDustTrailLeft.Stop();
		runningDustTrailRight.Clear();
		runningDustTrailLeft.Clear();
	}

	public void ToggleHealing(bool isHealing)
	{
		if (isHealing)
		{
			healingParticles.Play();
		} 
		else
		{
			healingParticles.Stop();
		}
	}
}
