﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class HideWall : MonoBehaviour
{
    public GameObject wall;
    public Rigidbody2D undead;
    private Boolean show = true;

    private bool isSpawned = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (wall.name == "big fake chest" && !isSpawned)
        {
            isSpawned = true;
            Rigidbody2D projectile = Instantiate(undead);
            projectile.transform.position = wall.transform.position + new Vector3(4f, 0, 0);
        }
    }

    private void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.CompareTag("Player")) 
        {
            show = false;
            Debug.Log("staying in");
        }
    }
    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.CompareTag("Player"))
        {
            show = true;
            Debug.Log("exited");
        }
        isSpawned = false;
    }

    private void Update() 
    {
        if (show && !wall.activeSelf)
            wall.SetActive(true);
        if (wall.activeSelf && show == false)
            wall.SetActive(false);

    }
}
