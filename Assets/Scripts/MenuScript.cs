﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class MenuScript : MonoBehaviour {

    private float musicVolume;
    private float soundFxVolume;

    public AudioMixer audioMixer;

    public void PlayGame()
    {
        SceneManager.LoadScene(3);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Back()
    {
        SceneManager.LoadScene(0);
    }

    public void Options()
    {
        //SceneManager.LoadScene(2);
    }

    public void SetMusicVolume(float volume)
    {
        audioMixer.SetFloat("MusicVolume", volume);
    }

    public void SetFxVolume(float volume)
    {
        audioMixer.SetFloat("SoundFxVolume", volume);
    }

    public void Locker()
    {
        SceneManager.LoadScene(1);
    }
}
