﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBoss : MonoBehaviour
{
    [SerializeField] private CameraSwitch camera;
    [SerializeField] private CovidBossController covidBoss;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            covidBoss.Spawn();
            camera.ShowBossStats();
            Destroy(GetComponent<BoxCollider2D>());
        }
    }
}
