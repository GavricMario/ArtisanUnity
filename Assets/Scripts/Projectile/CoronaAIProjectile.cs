﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class CoronaAIProjectile : BombBase
{
    [SerializeField] private int hitDamage;
    [SerializeField] private int poisonDamageOverTime;
    [SerializeField] private ParticleSystem poisonParticles;
    [SerializeField] private ParticleSystem destroyParticles;
    [Space]
    [SerializeField] private float seekPathRepeatRate = 2f;
    [SerializeField] private float nextWaypointDistance = 3f;
    [SerializeField] protected float speed = 400f;

    public GameObject target;

    private float poisonDuration = 7f;
    public bool shouldPoison = true;

    private Path path;
    private int currentWaypoint;
    private bool reachedEndOfPath = true;

    private Seeker seeker;
    private Rigidbody2D rb;

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        if (!exploded && !collision.CompareTag("Boss") && !collision.CompareTag("Projectile") && !collision.CompareTag("EnemyProjectile"))
        {
            base.OnTriggerEnter2D(collision);
            Explode(collision);
            CancelInvoke("UpdatePath");
            Destroy(transform.Find("Point Light 2D").gameObject);
        }
        else if (collision.CompareTag("Projectile"))
        {
            base.OnTriggerEnter2D(collision);
            destroyParticles.Play();
            Destroy(GetComponent<SpriteRenderer>());
            CancelInvoke("UpdatePath");
            Destroy(transform.Find("Point Light 2D").gameObject);
        }
    }

    protected override void Explode(Collider2D collision)
    {
        Destroy(GetComponent<SpriteRenderer>());
        if (collision.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerController>().OnDamage(hitDamage);
            collision.gameObject.GetComponent<PlayerController>().Poison(poisonDuration, poisonDamageOverTime);
        }
        if (shouldPoison)
        {
            poisonParticles.Play();
            StartCoroutine(ApplyPoisonDamage());
        }

    }

    IEnumerator ApplyPoisonDamage()
    {
        float timer = 0;
        while (timer < poisonDuration)
        {
            timer += 0.25f;
            Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, effectRadius, targets);
            foreach (Collider2D entity in colliders)
            {
                if (entity.CompareTag("Player"))
                {
                    Debug.Log("Hit");
                    Debug.Log("DamagedPlayer");
                    entity.GetComponent<PlayerController>().Poison(poisonDuration, poisonDamageOverTime);
                }
            }
            yield return new WaitForSeconds(0.25f);
        }
        yield return null;
    }


    private void Start()
    {
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();

        Debug.Log(target.name);

        InvokeRepeating("UpdatePath", 0f, seekPathRepeatRate);
    }

    private void FixedUpdate()
    {
        if (!exploded)
        {
            MoveToPath();
        }
    }

    private void UpdatePath()
    {
        if (seeker.IsDone())
        {
            seeker.StartPath(rb.position, target.transform.position, OnPathComplete);
        }
    }

    private void MoveToPath()
    {
        if (path == null)
        {
            return;
        }

        if (currentWaypoint >= path.vectorPath.Count)
        {
            reachedEndOfPath = true;
            return;
        }
        reachedEndOfPath = false;

        float distance;
        while (true)
        {
            distance = Vector2.Distance(rb.position, path.vectorPath[currentWaypoint]);
            if (distance < nextWaypointDistance)
            {
                if (currentWaypoint + 1 < path.vectorPath.Count)
                {
                    currentWaypoint++;
                }
                else
                {
                    reachedEndOfPath = true;
                    break;
                }
            }
            else
            {
                break;
            }
        }

        Vector2 direction = ((Vector2)path.vectorPath[currentWaypoint] - rb.position).normalized;
        Vector2 force = direction * speed * Time.deltaTime;

         rb.AddForce(force);
    }

    private void OnPathComplete(Path p)
    {
        p.Claim(this);
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
        else
        {
            p.Release(this);
        }
    }
}
