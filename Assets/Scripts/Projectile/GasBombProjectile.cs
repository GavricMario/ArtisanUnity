﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasBombProjectile : BombBase
{

    [SerializeField] private int hitDamage;
    [SerializeField] private int gasDamageOvertime;

    private float gasDuration = 2.5f;


    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        if (!exploded && !collision.CompareTag("Boss") && !collision.CompareTag("Enemy"))
        {
            base.OnTriggerEnter2D(collision);
            Explode(collision);
        }
    }

    protected override void Explode(Collider2D collision)
    {
        Destroy(transform.Find("Point Light 2D").gameObject);
        FindObjectOfType<AudioManager>().Play("PoisonBomb");
        Destroy(GetComponent<SpriteRenderer>(), gasDuration);
        transform.localScale = new Vector3(0.6f, 0.6f, 1);
        if (collision.CompareTag("Player"))
        {
            Debug.Log("Hit");
            collision.gameObject.GetComponent<PlayerController>().OnDamage(hitDamage);
        }

        StartCoroutine(ApplyGasDamage());

    }

    IEnumerator ApplyGasDamage()
    {
        float timer = 0;
        while (timer < gasDuration)
        {
            timer += 0.25f;
            bool isPlayerDamaged = false;
            Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, effectRadius, targets);
            foreach (Collider2D entity in colliders)
            {
                if (entity.CompareTag("Player") && !isPlayerDamaged)
                {
                    isPlayerDamaged = true;
                    Debug.Log("DamagedPlayer");
                    entity.GetComponent<PlayerController>().OnDamage(gasDamageOvertime);
                }
                else if (entity.CompareTag("Enemy"))
                {
                    entity.GetComponent<EnemyAIController>().OnDamage(gasDamageOvertime);
                }
            }
            yield return new WaitForSeconds(0.25f);
        }
        yield return null;
    }
}
