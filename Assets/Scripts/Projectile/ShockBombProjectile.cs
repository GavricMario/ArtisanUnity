﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShockBombProjectile : BombBase
{

    [SerializeField] private ParticleSystem shock1;
    [SerializeField] private ParticleSystem shock2;
    [SerializeField] private float shockDuration;
    [SerializeField] private int shockDamage;

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Projectile"))
        {
            base.OnTriggerEnter2D(collision);
            Destroy(transform.Find("Point Light 2D").gameObject);
            Destroy(GetComponent<SpriteRenderer>(), 0.7f);
        }
        if (!collision.CompareTag("Player") && !exploded)
        {
            base.OnTriggerEnter2D(collision);
            Destroy(transform.Find("Point Light 2D").gameObject);
            Explode(collision);
        }
    }

    protected override void Explode(Collider2D collision)
    {
        FindObjectOfType<AudioManager>().Play("ShockBomb");
        Destroy(GetComponent<SpriteRenderer>(), 0.7f);
        EmmitShock();

        Destroy(shock1, 1f);
        Destroy(shock2, 1f);

        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, effectRadius, targets);
        foreach (Collider2D entity in colliders)
        {
            if (entity.CompareTag("Enemy"))
            {
                try
                {
                    entity.GetComponent<EnemyAIController>().Shock(shockDuration, shockDamage);
                }
                catch (Exception) { }
                try
                {
                    entity.GetComponent<UndeadConroller>().Shock(shockDuration, shockDamage);
                }
                catch (Exception) { }
            }
            else if (entity.CompareTag("Boss"))
            {
                entity.GetComponent<BossAIController>().Shock(shockDuration, shockDamage);
            }
        }
    }

    private void EmmitShock()
    {
        ParticleSystem.EmitParams emitOverride = new ParticleSystem.EmitParams();
        emitOverride.startLifetime = 0.7f;
        shock1.Emit(emitOverride, 10);
        shock2.Emit(emitOverride, 10);
    }

}
