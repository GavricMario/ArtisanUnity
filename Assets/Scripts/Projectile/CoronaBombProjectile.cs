﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoronaBombProjectile : BombBase
{
    [SerializeField] private int hitDamage;
    [SerializeField] private int poisonDamageOverTime;
    [SerializeField] private ParticleSystem poisonParticles;
    [SerializeField] private ParticleSystem destroyParticles;

    private float poisonDuration = 7f;
    public bool shouldPoison = true;


    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        if (!exploded && !collision.CompareTag("Boss") && !collision.CompareTag("Projectile") && !collision.CompareTag("EnemyProjectile"))
        {
            base.OnTriggerEnter2D(collision);
            Destroy(transform.Find("Point Light 2D").gameObject);
            Explode(collision);
        }
        else if (collision.CompareTag("Projectile"))
        {
            base.OnTriggerEnter2D(collision);
            destroyParticles.Play();
            Destroy(GetComponent<SpriteRenderer>());
            Destroy(transform.Find("Point Light 2D").gameObject);
        }
    }

    protected override void Explode(Collider2D collision)
    {
        Destroy(GetComponent<SpriteRenderer>());
        if (collision.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerController>().OnDamage(hitDamage);
            collision.gameObject.GetComponent<PlayerController>().Poison(poisonDuration, poisonDamageOverTime);
        }
        if (shouldPoison)
        {
            poisonParticles.Play();
            StartCoroutine(ApplyPoisonDamage());
        }

    }

    IEnumerator ApplyPoisonDamage()
    {
        float timer = 0;
        while (timer < poisonDuration)
        {
            timer += 0.25f;
            Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, effectRadius, targets);
            foreach (Collider2D entity in colliders)
            {
                if (entity.CompareTag("Player"))
                {
                    Debug.Log("Hit");
                    Debug.Log("DamagedPlayer");
                    entity.GetComponent<PlayerController>().Poison(poisonDuration, poisonDamageOverTime);
                }
            }
            yield return new WaitForSeconds(0.25f);
        }
        yield return null;
    }
}
