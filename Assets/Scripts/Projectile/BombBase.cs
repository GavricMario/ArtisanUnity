﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BombBase : MonoBehaviour
{
    [SerializeField] protected LayerMask targets;
    [SerializeField] protected Animator animator;
    [SerializeField] protected float effectRadius = 5f;    

    protected bool exploded = false;

    void Start()
    {
        GetComponent<Rigidbody2D>().freezeRotation = true;
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (animator != null)
        {
            animator.SetTrigger("explode");
        }
        exploded = true;
        Destroy(GetComponent<Rigidbody2D>());
        Destroy(GetComponent<CircleCollider2D>());
        Destroy(gameObject, 5f);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, effectRadius);
    }

    protected abstract void Explode(Collider2D collision);
}
