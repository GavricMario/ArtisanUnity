﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    private float length, startpos, ystartingpos, zstartingpos;

    public GameObject cam;
    public float parallexEffect;
    void Start()
    {
        startpos = transform.position.x;
        ystartingpos = transform.position.y;
        zstartingpos = transform.position.z;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
        Debug.Log(length);
    }
    void Update()
    {
        float temp = (cam.transform.position.x * (1 - parallexEffect));
        float dist = (cam.transform.position.x * parallexEffect);
        transform.position = new Vector3(startpos + dist, ystartingpos, zstartingpos);
        if (temp > startpos + length) startpos += length;
        else if (temp < startpos - length) startpos -= length;
    }
}