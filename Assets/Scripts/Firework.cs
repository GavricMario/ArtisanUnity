﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Firework : MonoBehaviour
{
    [SerializeField] private ParticleSystem firework1;
    [SerializeField] private ParticleSystem firework2;
    [SerializeField] private ParticleSystem firework3;

    public void PlayFirework()
    {
        firework1.Play();
        firework2.Play();
        firework3.Play();
    }
}
