﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    [SerializeField] private CameraKeys camera;
    [SerializeField] private int keyNumber;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            camera.activateKey(keyNumber);
            Destroy(gameObject);
        }        
    }
}
