﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattBossController : BossAIController
{

    [SerializeField] private GameObject[] shootPosition;
    [SerializeField] private Rigidbody2D projectileObject;
    [SerializeField] private Rigidbody2D shootTarget;
    [SerializeField] private Rigidbody2D healOrb;
    [SerializeField] private GameObject deathSprite;
    [SerializeField] private GameObject deathDirection;
    [SerializeField] private ParticleSystem deathParticles;
    [SerializeField] private float projectileVelocity;
    [SerializeField] private CameraSwitch camera;

    private float attackTime = 0f;
    private float attackInit = 0f;

    private string attack = "";
    private float nextAttackDelay = 0f;

    private readonly string[] attacks = { "AttackOne", "AttackTwo", "AttackThree", "AttackFour" }; 

    protected override void Update()
    {
        base.Update();
        if (attackTime == 0)
        {
            attack = attacks[Random.Range(0, 4)];
            attackTime = Random.Range(3f + nextAttackDelay, 6f + nextAttackDelay);
            attackInit = Time.time;
        }

        if (attackTime != 0 && Time.time - attackTime > attackInit && !isShocked)
        {
            if (attack != "AttackFour")
            {
                animator.SetTrigger("attack");
            }                
            StartCoroutine(attack);
            attackTime = 0;
        }
    }

    IEnumerator AttackOne()
    {
        FindObjectOfType<AudioManager>().Play("BatShoot1");
        yield return new WaitForSeconds(0.8f);
        for (int i = 0; i < shootPosition.Length; i++) {
            if (isShocked)
            {
                break;
            }
            if (i % 2 == 0)
            {
                Rigidbody2D projectile = Instantiate(projectileObject);
                projectile.transform.position = shootPosition[i].transform.position;
                projectile.velocity = Vector2.down * projectileVelocity * 1.75f;
            }
        }
        yield return null;
    }

    IEnumerator AttackTwo()
    {
        FindObjectOfType<AudioManager>().Play("BatShoot1");
        yield return new WaitForSeconds(0.8f);
        for (int i = 0; i < shootPosition.Length; i++)
        {
            if (isShocked)
            {
                break;
            }
            if (i % 2 == 0)
            {
                yield return new WaitForSeconds(0.15f);
                Rigidbody2D projectile = Instantiate(projectileObject);
                projectile.transform.position = shootPosition[i].transform.position;
                projectile.velocity = Vector2.down * projectileVelocity;
            }
        }
        if (isShocked)
        {
            animator.SetTrigger("attack");
            yield return new WaitForSeconds(0.8f);
        }
        FindObjectOfType<AudioManager>().Play("BatShoot1");
        for (int i = shootPosition.Length - 1; i >= 0; i--)
        {
            if (isShocked)
            {
                break;
            }
            if (i % 2 == 1)
            {
                yield return new WaitForSeconds(0.15f);
                Rigidbody2D projectile = Instantiate(projectileObject);
                projectile.transform.position = shootPosition[i].transform.position;
                projectile.velocity = Vector2.down * projectileVelocity;
            }
        }
        yield return null;
    }

    IEnumerator AttackThree()
    {
        FindObjectOfType<AudioManager>().Play("BatShoot1");
        yield return new WaitForSeconds(0.8f);
        for (int i = 0; i < shootPosition.Length; i++)
        {
            if (isShocked)
            {
                break;
            }
            Rigidbody2D projectile = Instantiate(projectileObject);
            projectile.transform.position = shootPosition[i].transform.position;

            float angle = shootPosition[i].transform.rotation.eulerAngles.z;
            if (angle > 180)
            {
                angle -= 360;
            }
            float x = shootPosition[i].transform.position.x + Mathf.Cos((-90 + angle) * Mathf.Deg2Rad);
            float y = shootPosition[i].transform.position.y + Mathf.Sin((-90 + angle) * Mathf.Deg2Rad);
            Vector2 heading = (new Vector3(x, y, 0f) - shootPosition[i].transform.position);
            float distance = heading.magnitude;
            Vector2 direction = heading / distance;
            projectile.velocity = direction * projectileVelocity * 1.25f;
        }
        yield return null;
    }

    IEnumerator AttackFour()
    {
        nextAttackDelay = 8f;
        for (int i = 0; i < shootPosition.Length; i++)
        {
            if (isShocked)
            {
                break;
            }
            FindObjectOfType<AudioManager>().Play("BatShoot2");
            animator.SetTrigger("attack");
            yield return new WaitForSeconds(0.9f);
            float multiply = (transform.position.x - shootTarget.position.x) * 1.5f;
            Rigidbody2D projectile1 = Instantiate(projectileObject);
            projectile1.transform.position = shootPosition[1].transform.position;
            projectile1.transform.localScale = new Vector3(0.6f, 0.6f, 1);
            Vector2 heading1 = (new Vector3(shootTarget.transform.position.x - 1 - multiply, shootTarget.transform.position.y) - shootPosition[1].transform.position);
            float distance1 = heading1.magnitude;
            Vector2 direction1 = heading1 / distance1;
            projectile1.GetComponent<Rigidbody2D>().gravityScale = 0.5f;
            projectile1.velocity = direction1 * projectileVelocity * 1.25f;

            Rigidbody2D projectile2 = Instantiate(projectileObject);
            projectile2.transform.position = shootPosition[7].transform.position;
            projectile2.transform.localScale = new Vector3(0.6f, 0.6f, 1);
            Vector2 heading2 = (new Vector3(shootTarget.transform.position.x + 1 - multiply, shootTarget.transform.position.y) - shootPosition[7].transform.position);
            float distance2 = heading2.magnitude;
            Vector2 direction2 = heading2 / distance2;
            projectile2.GetComponent<Rigidbody2D>().gravityScale = 0.5f;
            projectile2.velocity = direction2 * projectileVelocity * 1.25f;

            Rigidbody2D projectile3 = Instantiate(projectileObject);
            projectile3.transform.position = shootPosition[4].transform.position;
            projectile3.transform.localScale = new Vector3(0.5f, 0.5f, 1);            
            Vector2 heading3 = (new Vector3(shootTarget.transform.position.x - multiply, shootTarget.transform.position.y) - shootPosition[4].transform.position);
            float distance3 = heading3.magnitude;
            Vector2 direction3 = heading3 / distance3;
            projectile3.GetComponent<Rigidbody2D>().gravityScale = 0.5f;
            projectile3.velocity = direction3 * projectileVelocity * 1.6f;
        }
        nextAttackDelay = 0f;
        yield return null;
    }

    protected override void Die()
    {
        int numberOfOrb = Random.Range(2, 6);
        for (int i = 0; i < numberOfOrb; i++)
        {
            Rigidbody2D projectile = Instantiate(healOrb);
            projectile.transform.position = shootPosition[Random.Range(0, 8)].transform.position;
        }
        FindObjectOfType<AudioManager>().Stop("ShockBomb");
        FindObjectOfType<AudioManager>().Play("BatDeath");
        FindObjectOfType<AudioManager>().Stop("Background");
        FindObjectOfType<AudioManager>().Play("Background2");
        camera.SwitchCamera(2);
        deathSprite.transform.position = transform.position;
        Destroy(GetComponent<PolygonCollider2D>());
        deathParticles.Play();
        deathSprite.SetActive(true);
        Vector3 add = new Vector3((deathDirection.transform.position.x - deathSprite.transform.position.x) * 2 , 0f, 0f);
        Vector2 heading = (deathDirection.transform.position + add - deathSprite.transform.position);
        float distance = heading.magnitude;
        Vector2 direction = heading / distance;
        deathSprite.GetComponent<Rigidbody2D>().velocity = direction * 10;
        Destroy(GetComponent<SpriteRenderer>());
        Destroy(gameObject, 3);
        Destroy(GetComponent<BattBossController>());
    }
}
