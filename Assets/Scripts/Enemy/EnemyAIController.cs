﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class EnemyAIController : MonoBehaviour
{
    [SerializeField] protected Animator animator;
    [SerializeField] protected GameObject target;
    [Space]
    [SerializeField] private float seekPathRepeatRate = 1f;
    [SerializeField] private float nextWaypointDistance = 3f;
    [SerializeField] protected float speed = 200f;
    [SerializeField] private float followRadius = 20;
    [Space]
    [SerializeField] protected float attackCooldown;
    [SerializeField] protected float attackDamage;
    [Space]
    [SerializeField] private int maxHP = 100;

    protected float lastAttackTime = Mathf.NegativeInfinity;
    private int currentHP;

    protected bool isShocked = false;
    private float shockDuration;

    private Path path;
    private int currentWaypoint;
    private bool reachedEndOfPath = true;
    private bool movedLeft = false;

    private Seeker seeker;
    private Rigidbody2D rb;

    private void Start()
    {
        currentHP = maxHP;
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();

        InvokeRepeating("UpdatePath", 0f, seekPathRepeatRate);
    }

    private void FixedUpdate()
    {
        Bounds bounds = GetComponent<PolygonCollider2D>().bounds;
        AstarPath.active.UpdateGraphs(bounds);
        bounds.Expand(3);

        MoveToPath();
    }

    private void UpdatePath()
    {
        if (seeker.IsDone())
        {
            if (Vector2.Distance(target.transform.position, rb.position) < followRadius && Vector2.Distance(target.transform.position, rb.position) > 3f)
            {
                seeker.StartPath(rb.position, target.transform.position, OnPathComplete);
            } 
            else if (Vector2.Distance(target.transform.position, rb.position) < 3f)
            {
                Vector2 randomPoint;
                if (rb.position.x - target.transform.position.x > 0)
                {
                    randomPoint = new Vector2(Random.Range(rb.position.x - 4f, rb.position.x), rb.position.y + Random.Range(-1f, 1f));
                } else if (rb.position.x - target.transform.position.x < 0)
                {
                    randomPoint = new Vector2(Random.Range(rb.position.x, rb.position.x + 4f), rb.position.y + Random.Range(-1f, 1f));
                }
                else
                {
                    randomPoint = new Vector2(Random.Range(rb.position.x - 5f, rb.position.x + 5f), rb.position.y + Random.Range(-1f, 1f));
                }
                seeker.StartPath(rb.position, randomPoint, OnPathComplete);
            }
        }
    }

    private void MoveToPath()
    {
        if (path == null || isShocked)
        {
            rb.velocity = Vector3.zero;
            return;
        }

        if (currentWaypoint >= path.vectorPath.Count)
        {
            reachedEndOfPath = true;
            return;
        }
        reachedEndOfPath = false;

        if (!isShocked)
        {
            float distance;
            while (true)
            {
                distance = Vector2.Distance(rb.position, path.vectorPath[currentWaypoint]);
                if (distance < nextWaypointDistance)
                {
                    if (currentWaypoint + 1 < path.vectorPath.Count)
                    {
                        currentWaypoint++;
                    }
                    else
                    {
                        reachedEndOfPath = true;
                        break;
                    }
                }
                else
                {
                    break;
                }
            }

            float speedFactor = reachedEndOfPath ? Mathf.Sqrt(distance / nextWaypointDistance) : 1f;
            Vector2 direction = ((Vector2)path.vectorPath[currentWaypoint] - rb.position).normalized;
            Vector2 force = direction * speed * speedFactor * Time.deltaTime;

            rb.AddForce(force);
        }
    }

    private void OnPathComplete(Path p)
    {
        p.Claim(this);
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
        else
        {
            Debug.Log(p.errorLog);
            p.Release(this);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(rb.position, followRadius);
        Gizmos.DrawWireSphere(rb.position, 3f);

        Gizmos.DrawLine(target.transform.position + new Vector3(-0.35f, -3.5f, 0f), target.transform.position + new Vector3(0.35f, -3.5f, 0f));
        Gizmos.DrawLine(target.transform.position + new Vector3(0.35f, -3.5f, 0f), target.transform.position + new Vector3(0.35f, 3.5f, 0f));
        Gizmos.DrawLine(target.transform.position + new Vector3(0.35f, 3.5f, 0f), target.transform.position + new Vector3(-0.35f, 3.5f, 0f));
        Gizmos.DrawLine(target.transform.position + new Vector3(-0.35f, 3.5f, 0f), target.transform.position + new Vector3(-0.35f, -3.5f, 0f));
    }

    protected virtual void Attack()
    {
        lastAttackTime = Time.time;
        animator.SetTrigger("attack");
    }

    public void Shock(float duration, int damage)
    {
        isShocked = true;
        shockDuration = duration;
        OnDamage(damage);
        StartCoroutine(Shocked());
    }

    IEnumerator Shocked()
    {
        yield return new WaitForSeconds(shockDuration);
        isShocked = false;
        yield return null;
    }

    public void OnDamage(float damage)
    {
        currentHP -= (int)damage;
        OnHealthChanged();
    }

    public void OnDamage(int damage)
    {
        currentHP -= damage;
        OnHealthChanged();
    }

    private void OnHealthChanged()
    {
        Debug.Log("Shocked");

        if (currentHP <= 0)
        {
            Die();
        }
    }

    protected virtual void Die()
    {
        Destroy(GetComponent<EnemyAIController>());
        Destroy(gameObject, 0.5f);
    }
}
