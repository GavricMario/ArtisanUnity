﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UndeadConroller : MonoBehaviour
{
	[Header("Movement")]
	[SerializeField] public float walkSpeed = 20f;
	[SerializeField] public float fallDamageSpeed = 10f;
	[Space]
	[Header("Stats")]
	[SerializeField] private int maxHP = 100;
	[Space]
	[Header("Attacking")]
	[SerializeField] public float attackCooldown;
	[SerializeField] private float attackRange = 4f;
	[SerializeField] private float attackDamage = 40f;
	[SerializeField] private LayerMask enemyLayers;
	[SerializeField] private GameObject target;
	[Space]
	[Header("Objects")]
	[SerializeField] private Animator animator;
	[SerializeField] private Transform attackPoint;
	[SerializeField] private LayerMask m_WhatIsGround;
	[SerializeField] private Transform edgeCheck;
	[SerializeField] private Transform wallCheck;

	private int currentHP;
	private float fallingSpeed = 0f;
	private bool atEdge = false;
	private float direction = 1f;

	private bool isTakingDamage = false;

	private bool followingPlayer = false;

	private float lastAttack = Mathf.NegativeInfinity;
	private bool isAttacking = false;

	const float edgeRadius = .25f;

	private Rigidbody2D m_Rigidbody2D;
	private bool m_FacingRight = true;
	private bool isShocked;
	private float shockDuration;

	private void Start()
	{
		currentHP = maxHP;
	}

	private void Awake()
	{
		m_Rigidbody2D = GetComponent<Rigidbody2D>();
	}

	private void Update()
	{
		if (!isShocked)
		{
			Move(!isAttacking);
			CheckPlayer();
			CheckEdge();
			CheckedFalling();
			CheckAttack();
		}
	}
	private void CheckPlayer()
	{
		if (Physics2D.OverlapCircle(transform.position, 4f, enemyLayers))
		{
			followingPlayer = true;
			if (target.transform.position.x < transform.position.x)
			{
				direction = -1;
			}
			else
			{
				direction = 1;
			}

		}else
		{
			followingPlayer = false;
		}
	}

	private void CheckedFalling()
	{
		if (m_Rigidbody2D.velocity.y < 0)
		{
			fallingSpeed = m_Rigidbody2D.velocity.y;
		}

		if (fallingSpeed != 0 && m_Rigidbody2D.velocity.y >= 0)
		{
			Landed();
		}

	}

	private void CheckEdge()
	{
		if (!Physics2D.OverlapCircle(edgeCheck.position, edgeRadius, m_WhatIsGround) && m_Rigidbody2D.velocity.y == 0 && fallingSpeed == 0)
		{
			atEdge = true;
		}
		else if (Physics2D.OverlapCircle(wallCheck.position, edgeRadius, m_WhatIsGround) && m_Rigidbody2D.velocity.y == 0 && fallingSpeed == 0)
		{
			atEdge = true;
		}
		else
		{
			atEdge = false;
		}
	}

	private void CheckAttack()
	{
		if (Physics2D.OverlapCircle(attackPoint.position, attackRange, enemyLayers) && Time.time - lastAttack > attackCooldown && !isTakingDamage)
		{
			animator.SetBool("isAttacking", true);
			animator.SetTrigger("attack");
			Invoke("Attack", 0.5f);
			lastAttack = Time.time;
			isAttacking = true;
		}
	}

	public void Move(bool isRunning)
	{
		animator.SetBool("isRunning", isRunning);

		if (atEdge && !followingPlayer)
		{
			atEdge = false;
			direction *= -1;
		}

		if (!atEdge)
		{
			Vector3 targetVelocity = new Vector2(walkSpeed * direction, m_Rigidbody2D.velocity.y);
			m_Rigidbody2D.velocity = targetVelocity;
		}

		// Check player rotation
		if (direction > 0 && !m_FacingRight)
		{
			Flip();
		}
		else if (direction < 0 && m_FacingRight)
		{
			Flip();
		}

	}

	private void Landed()
	{
		if (Mathf.Abs(fallingSpeed) > fallDamageSpeed)
		{
			Debug.Log(Mathf.Abs(fallingSpeed));
			Debug.Log(fallDamageSpeed);
			Debug.Log((Mathf.Abs(fallingSpeed) / fallDamageSpeed));
			OnDamage(((Mathf.Abs(fallingSpeed) / fallDamageSpeed) - 1) * 100);
		}
		fallingSpeed = 0;
	}

	public void OnDamage(float damage)
	{
		if (currentHP > 0 && !isTakingDamage)
		{
			Debug.Log("HERE1");
			isTakingDamage = true;
			currentHP -= (int)damage;
			OnHealthChanged(true);
			Invoke("ResetDamage", 0.5f);
		}
	}

	public void OnDamage(int damage)
	{
		if (currentHP > 0 && !isTakingDamage)
		{
			isTakingDamage = true;
			currentHP -= damage;
			OnHealthChanged(true);
			Invoke("ResetDamage", 0.5f);
		}
	}

	private void ResetDamage()
	{
		isTakingDamage = false;
	}

	private void OnHealthChanged(bool hasTakenDamage)
	{
		if (currentHP < 0)
		{
			currentHP = 0;
		}
		if (currentHP > maxHP)
		{
			currentHP = maxHP;
		}

		if (currentHP < 1)
		{
			//TODO show game over menu
			animator.SetTrigger("isDead");
			m_Rigidbody2D.velocity = Vector3.zero;
			Destroy(GetComponent<UndeadConroller>());
			Destroy(gameObject,1f);
		}
		else if (hasTakenDamage)
		{
			Debug.Log("HERE");
			animator.SetTrigger("damage");
		}


	}

	public void Attack()
	{
		FindObjectOfType<AudioManager>().Play("PlayerDeath");
		bool playerHit = false;
		Collider2D[] hitPlayer = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);

		foreach (Collider2D player in hitPlayer)
		{
			if (player.CompareTag("Player") && !playerHit)
			{
				playerHit = true;
				try
				{
					player.GetComponent<PlayerController>().OnDamage(attackDamage);
				}
				catch (Exception) { }
			}
		}

		isAttacking = false;
		animator.SetBool("isAttacking", false);
	}

	private void OnDrawGizmosSelected()
	{
		if (attackPoint == null) return;
		Gizmos.DrawWireSphere(attackPoint.position, attackRange);
		Gizmos.DrawWireSphere(edgeCheck.position, edgeRadius);
		Gizmos.DrawWireSphere(wallCheck.position, edgeRadius);
		Gizmos.DrawWireSphere(transform.position, 4f);
	}

	private void Flip()
	{
		// Switch the way the player is labelled as facing.
		m_FacingRight = !m_FacingRight;

		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	public void Shock(float duration, int damage)
	{
		isShocked = true;
		shockDuration = duration;
		OnDamage(damage);
		StartCoroutine(Shocked());
	}

	IEnumerator Shocked()
	{
		yield return new WaitForSeconds(shockDuration);
		isShocked = false;
		yield return null;
	}

}
