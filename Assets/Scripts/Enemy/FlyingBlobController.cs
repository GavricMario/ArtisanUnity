﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingBlobController : EnemyAIController
{
    [SerializeField] private GameObject shootPosition;
    [SerializeField] private Rigidbody2D projectileObject;
    [SerializeField] private Rigidbody2D healOrb;
    [SerializeField] private float projectileVelocity;

    private void Update()
    {
        if (Mathf.Abs(shootPosition.transform.position.x - target.transform.position.x) < 0.35f && Mathf.Abs(shootPosition.transform.position.y - target.transform.position.y) < 3.5f && !isShocked)
        {
            Attack();
        }
    }

    IEnumerator Throw()
    {
        yield return new WaitForSecondsRealtime(0.65f);
        Rigidbody2D projectile = Instantiate(projectileObject);
        projectile.transform.position = shootPosition.transform.position;
        projectile.velocity = Vector2.down * projectileVelocity;
        yield return null;
    }

    protected override void Attack()
    {
        if (Time.time - attackCooldown >= lastAttackTime)
        {
            base.Attack();
            StartCoroutine(Throw());
        }
    }

    protected override void Die()
    {
        base.Die();
        Rigidbody2D projectile = Instantiate(healOrb);
        projectile.transform.position = shootPosition.transform.position;
    }
}
