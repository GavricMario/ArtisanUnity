﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public abstract class BossAIController : MonoBehaviour
{
    [SerializeField] protected Animator animator;
    [SerializeField] protected GameObject target;
    [Space]
    [SerializeField] private float seekPathRepeatRate = 3f;
    [SerializeField] private float nextWaypointDistance = 3f;
    [SerializeField] protected float speed = 200f;
    [Space]
    [SerializeField] protected float firstAttackDamage;
    [SerializeField] protected float secondAttackDamage;
    [SerializeField] protected float shockAmountBeforFall;
    [SerializeField] protected float shockResetTime;
    [SerializeField] private Bar ShockBar;
    [Space]
    [SerializeField] private int maxHP = 100;
    [SerializeField] private Bar healthBar;

    protected bool useRestriction = false;
    protected bool shouldFall = true;

    protected float lastAttackTime = Mathf.NegativeInfinity;
    private int currentHP;

    protected bool isShocked = false;
    private float shockDuration = 0f;
    private float lastShock = 0f;

    private Path path;
    private int currentWaypoint;
    private bool reachedEndOfPath = true;

    private Seeker seeker;
    private Rigidbody2D rb;

    private void Start()
    {
        currentHP = maxHP;
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();

        InvokeRepeating("UpdatePath", 0f, seekPathRepeatRate);
    }

    protected virtual void Update()
    {
        if (Time.time - lastShock > shockResetTime && !isShocked)
        {
            shockDuration = 0;
            OnShockChanged();
        }
        else if (shockDuration >= shockAmountBeforFall)
        {
            isShocked = true;
            animator.SetBool("isShocked", true);
            StartCoroutine(Shocked());
        }
    }

    private void FixedUpdate()
    {
        Bounds bounds = GetComponent<PolygonCollider2D>().bounds;
        AstarPath.active.UpdateGraphs(bounds);
        bounds.Expand(3);

        if (currentHP > 0)
        {
            MoveToPath();
        }
    }

    private void UpdatePath()
    {
        if (seeker.IsDone())
        {
            if (!useRestriction)
            {
                seeker.StartPath(rb.position, new Vector2(target.transform.position.x + Random.Range(-1, 1), target.transform.position.y + Random.Range(-2, 2)), OnPathComplete);
            }
            else
            {
                ABPath newPath = ABPath.Construct(rb.position, new Vector2(target.transform.position.x + Random.Range(-1, 1), target.transform.position.y + Random.Range(-2, 2)), null);

                newPath.calculatePartial = true;
                newPath.traversalProvider = GridShapeTraversalProvider.SquareShape(3);

                seeker.StartPath(newPath, OnPathComplete);
            }
        }
    }

    private void MoveToPath()
    {
        if (path == null || isShocked)
        {
            return;
        }

        if (currentWaypoint >= path.vectorPath.Count)
        {
            reachedEndOfPath = true;
            return;
        }
        reachedEndOfPath = false;

        if (!isShocked)
        {
            float distance;
            while (true)
            {
                distance = Vector2.Distance(rb.position, path.vectorPath[currentWaypoint]);
                if (distance < nextWaypointDistance)
                {
                    if (currentWaypoint + 1 < path.vectorPath.Count)
                    {
                        currentWaypoint++;
                    }
                    else
                    {
                        reachedEndOfPath = true;
                        break;
                    }
                }
                else
                {
                    break;
                }
            }

            float speedFactor = reachedEndOfPath ? Mathf.Sqrt(distance / nextWaypointDistance) : 1f;
            Vector2 direction = ((Vector2)path.vectorPath[currentWaypoint] - rb.position).normalized;
            Vector2 force = direction * speed * speedFactor * Time.deltaTime;

            rb.AddForce(force);
        }
    }

    private void OnPathComplete(Path p)
    {
        p.Claim(this);
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
        else
        {
            p.Release(this);
        }
    }

    protected virtual void Attack()
    {
        lastAttackTime = Time.time;
        animator.SetTrigger("attack");
    }

    public virtual void Shock(float duration, int damage)
    {
        if (!isShocked)
        {
            shockDuration += duration;
            OnShockChanged();
            lastShock = Time.time;
            if (shouldFall)
            {
                rb.velocity = Vector3.zero;
            }
            OnDamage(damage);
        }
    }

    IEnumerator Shocked()
    {
        rb.velocity = Vector2.down * 10;
        yield return new WaitForSeconds(shockDuration);
        isShocked = false;
        shockDuration = 0;
        animator.SetBool("isShocked", false);
        yield return null;
    }

    public void OnDamage(float damage)
    {
        currentHP -= (int)damage;
        OnHealthChanged();
    }

    public void OnDamage(int damage)
    {
        currentHP -= damage;
        OnHealthChanged();
    }

    protected void OnHealthChanged()
    {
        if (currentHP < 0)
        {
            currentHP = 0;
        }
        if (currentHP > maxHP)
        {
            currentHP = maxHP;
        }

        healthBar.SetSize((float)currentHP / maxHP);

        if (currentHP <= 0)
        {
            Die();
        }
        else
        {
            //TODO drop health blob 
        }
    }

    protected void OnShockChanged()
    {
        if (shockDuration < 0)
        {
            shockDuration = 0;
        }
        if (shockDuration > shockAmountBeforFall)
        {
            shockDuration = shockAmountBeforFall;
        }

        ShockBar.SetSize((float)shockDuration / shockAmountBeforFall);
    }

    protected abstract void Die();
}

class GridShapeTraversalProvider : ITraversalProvider
{
    Int2[] shape;

    public static GridShapeTraversalProvider SquareShape(int width)
    {
        if ((width % 2) != 1) throw new System.ArgumentException("only odd widths are supported");
        var shape = new GridShapeTraversalProvider();
        shape.shape = new Int2[width * width];

        // Create an array containing all integer points within a width*width square
        int i = 0;
        for (int x = -width / 2; x <= width / 2; x++)
        {
            for (int z = -width / 2; z <= width / 2; z++)
            {
                shape.shape[i] = new Int2(x, z);
                i++;
            }
        }
        return shape;
    }

    public bool CanTraverse(Path path, GraphNode node)
    {
        GridNodeBase gridNode = node as GridNodeBase;

        // Don't do anything special for non-grid nodes
        if (gridNode == null) return DefaultITraversalProvider.CanTraverse(path, node);
        int x0 = gridNode.XCoordinateInGrid;
        int z0 = gridNode.ZCoordinateInGrid;
        var grid = gridNode.Graph as GridGraph;

        // Iterate through all the nodes in the shape around the current node
        // and check if those nodes are also traversable.
        for (int i = 0; i < shape.Length; i++)
        {
            var inShapeNode = grid.GetNode(x0 + shape[i].x, z0 + shape[i].y);
            if (inShapeNode == null || !DefaultITraversalProvider.CanTraverse(path, inShapeNode)) return false;
        }
        return true;
    }

    public uint GetTraversalCost(Path path, GraphNode node)
    {
        // Use the default traversal cost.
        // Optionally this could be modified to e.g taking the average of the costs inside the shape.
        return DefaultITraversalProvider.GetTraversalCost(path, node);
    }
}