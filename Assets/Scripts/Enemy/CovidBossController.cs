﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class CovidBossController : BossAIController
{

    [SerializeField] private GameObject shootPosition;
    [SerializeField] private GameObject shootTarget;
    [SerializeField] private Rigidbody2D projectileObject;
    [SerializeField] private Rigidbody2D projectileObjectAI;
    [SerializeField] private ParticleSystem deathParticles;
    [SerializeField] private float projectileVelocity;
    [SerializeField] private GameHandler handler;
    [SerializeField] private Firework firework;

    private float attackTime = 0f;
    private float attackInit = 0f;

    private bool isDead = false;

    private string attack = "";    

    private float scale = 0f;

    private readonly string[] attacks = { "AttackOne", "AttackTwo"};

    private void Awake()
    {
        useRestriction = true;
        shouldFall = false;
    }

    protected override void Update()
    {
        if (!isDead)
        {
            base.Update();
            if (attackTime == 0)
            {
                attack = attacks[Random.Range(0, 1)];
                attackTime = Random.Range(3f, 6f);
                attackInit = Time.time;
            }

            if (attackTime != 0 && Time.time - attackTime > attackInit && !isShocked)
            {
                if (attack == "AttackOne" && Mathf.Abs(target.transform.position.x - shootPosition.transform.position.x) > 5f)
                {
                    attack = "AttackTwo";
                }
                else if (Vector2.Distance(target.transform.position, shootPosition.transform.position) < 2f)
                {
                    attack = "AttackOne";
                }
                StartCoroutine(attack);
                attackTime = 0;
            }
        }
        
    }

    public void Spawn()
    {
        gameObject.SetActive(true);
        StartCoroutine(AwakeCorona());
    }

    IEnumerator AwakeCorona()
    {
        while (scale < 2)
        {
            scale += 0.15f;
            transform.localScale = new Vector2(scale, scale);
            yield return new WaitForSeconds(0.2f);
        }
        OnHealthChanged();
        OnShockChanged();
        yield return null;
    }

    IEnumerator AttackOne()
    {
        for (int i = 0; i < 50; i++)
        {
            FindObjectOfType<AudioManager>().Play("CovidAttack");
            animator.SetTrigger("attack");
            yield return new WaitForSeconds(0.075f);
            Rigidbody2D projectile = Instantiate(projectileObject);
            projectile.GetComponent<CoronaBombProjectile>().shouldPoison = false;
            projectile.transform.position = shootPosition.transform.position;
            projectile.GetComponent<Rigidbody2D>().gravityScale = 0f;
            var point = Random.insideUnitSphere * 8f;
            Vector2 heading = (new Vector3(target.transform.position.x + point.x, target.transform.position.y + point.y, 0f) - shootPosition.transform.position);
            float distance = heading.magnitude;            
            Vector2 direction = heading / distance;
            projectile.velocity = direction * projectileVelocity * 3.5f;
            Destroy(projectile.gameObject, 0.75f);
        }
        yield return null;
    }

    IEnumerator AttackTwo()
    {
        yield return new WaitForSeconds(1f);
        animator.SetTrigger("attack");
        for (int i = 0; i < 3; i++)
        {
            FindObjectOfType<AudioManager>().Play("CovidAttack");
            yield return new WaitForSeconds(0.15f);
            Rigidbody2D projectile = Instantiate(projectileObjectAI);
            projectile.GetComponent<CoronaAIProjectile>().target = shootTarget;
            projectile.velocity = Vector3.zero;
            projectile.transform.position = shootPosition.transform.position;
            Destroy(projectile.gameObject, 10f);
        }
        yield return null;
    }

    public override void Shock(float duration, int damage)
    {
        base.Shock(duration, damage);
        if (isShocked)
        {
            attackTime += 10f;
        }
    }

    protected override void Die()
    {
        isDead = true;
        FindObjectOfType<AudioManager>().Play("CovidDeath");
        FindObjectOfType<AudioManager>().Stop("Background3");
        FindObjectOfType<AudioManager>().Play("CLAP");
        Destroy(GetComponent<PolygonCollider2D>());
        deathParticles.Play();
        Destroy(GetComponent<SpriteRenderer>());
        firework.PlayFirework();
        Invoke("NextScene", 20f);
    }

    private void NextScene()
    {
        handler.GoToNextScene(2);
        Destroy(gameObject);
    }
}