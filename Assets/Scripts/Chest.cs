﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour
{

    [SerializeField] private GameObject Key;
    [SerializeField] private int health;

    private int currentHP;


    private void Start()
    {
        currentHP = health;
    }
    public void OnDamage(float damage)
    {
        currentHP -= (int)damage;

        if (currentHP <= 0)
        {
            Key.SetActive(true);
            Destroy(GetComponent<SpriteRenderer>());
            Destroy(GetComponent<BoxCollider2D>());
            Destroy(transform.Find("Parametric Light 2D").gameObject);
        }
    }

    
}
