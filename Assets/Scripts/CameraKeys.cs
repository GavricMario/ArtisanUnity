﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraKeys : MonoBehaviour
{
    [SerializeField] private SpriteRenderer key1;
    [SerializeField] private SpriteRenderer key2;
    [SerializeField] private GameHandler handler;

    private int keyNumber = 0;

    public void activateKey(int key)
    {
        if (key == 1)
        {
            key1.color = Color.white;
            keyNumber ++;
        }
        else
        {
            key2.color = new Color(1f, 0.6239311f, 0f);
            keyNumber++;
        }

        if (keyNumber == 2)
        {
            handler.OpenTrapDoor();
        }
    }
}
