﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformExplode : MonoBehaviour
{
    [SerializeField] private ParticleSystem explodeParticles;
    [SerializeField] private bool projectileDamage = true;

    private bool exploded = false;
    private float health = 200;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!exploded)
        {
            if (collision.CompareTag("Projectile") && projectileDamage)
            {
                health -= 10;
                if (health <= 0)
                {
                    Explode();
                }
            }
            else if (collision.name == "BossDead")
            {
                health = 0;
                Explode();
            }
        }
    }

    private void Explode()
    {
        exploded = true;
        BoxCollider2D[] colliders = GetComponents<BoxCollider2D>();
        foreach (BoxCollider2D collider in colliders)
        {
            Destroy(collider);
        }
        SpriteRenderer[] renderers = GetComponentsInChildren<SpriteRenderer>();
        foreach (SpriteRenderer renderer in renderers)
        {
            Destroy(renderer, 0.1f);
        }
        explodeParticles.Play();
        Destroy(gameObject, 0.4f);

    }

}
