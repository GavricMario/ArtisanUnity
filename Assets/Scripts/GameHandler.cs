﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using Pathfinding;

public class GameHandler : MonoBehaviour
{
    [SerializeField] private TrapDoor trapDoor;

    public void OpenTrapDoor()
    {
        trapDoor.OpenDoor();
    }

    public void GoToNextScene(int scene)
    {
        if (scene == 1) SceneManager.LoadScene(4);
        else SceneManager.LoadScene(3);
    }
    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
    }
}
