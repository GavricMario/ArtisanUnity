﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class ShowChest : MonoBehaviour
{
    public GameObject chest;
    private Boolean show = false;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("Player"))
        {
            show = true;
        }
    }
    private void Update()
    {
        if (show)
            chest.SetActive(true);
        if (!show)
            chest.SetActive(false);

    }
}
